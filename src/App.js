import React, { Component } from 'react';
import Checkout from './components/Checkout';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Checkout />
      </div>
    );
  }
}

export default App;
