export const applyPromo = (promoCode) => {
  return { type: 'APPLY_PROMO', payload: promoCode }
}
