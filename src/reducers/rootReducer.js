import adaraProduct1 from '../assets/images/ADARA-058_1080x.jpg';
import adaraProduct2 from '../assets/images/ADARA-028_540x.jpg';

const defaultState = {
  discounts: [
    {promoCode: 'PICKUP_SAVINGS', amount: 10.00, isPercent: false}
  ],
  products: [
    {id: 1, name: 'CHIC WHITE TOP - Adara', price: 69.70, img: adaraProduct1, quantity: 1},
    {id: 2, name: 'MUSTARD MAXI TUNIC - Adara', price: 110.70, img: adaraProduct2, quantity: 2},
  ],
  tax: .07,
  shippingFee: 12
}

const rootReducer = (state = defaultState, action) => {

  if(action.type === 'APPLY_PROMO') {
    if(action.payload === 'DISCOUNT') {
      return {
        ...state,
        discounts: [...state.discounts, {promoCode: 'DISCOUNT', amount: .10, isPercent: true}]
      }
    }
  }
  return state;

}

export default rootReducer