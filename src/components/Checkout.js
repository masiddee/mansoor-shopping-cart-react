import React, { Component } from 'react';
import { Card, Collapse } from 'react-bootstrap';
import '@fortawesome/fontawesome-free/css/all.min.css';
// import "bootstrap-css-only/css/bootstrap.min.css";
// import "mdbreact/dist/css/mdb.css";
import { MDBIcon } from 'mdbreact';
import Discount from './Discount';
import ItemList from './ItemList';
import Total from './Total';
import { connect } from 'react-redux';

class Checkout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      detailsOpen: false,
      discountOpen: false
    }
  }

  render() {
    return (
      <Card style={{ minWidth: '350px', width: '20%', margin: '0 auto' }}>
        <Card.Header as="h4">Shopping Cart</Card.Header>
        <Card.Body>

          <Total 
            discounts={ this.props.discounts }
            products={ this.props.products }
            tax={ this.props.tax }
            shippingFee={ this.props.shippingFee } />

        </Card.Body>
        <Card.Body>
          <Card.Title as="h6">Show Item Details<i><MDBIcon icon="plus"
            onClick={() => this.setState({ detailsOpen: !this.state.detailsOpen })} 
            aria-controls="collapse-item-details"
            aria-expanded={ this.state.detailsOpen } className="ml-2" /></i></Card.Title>
          <Collapse in={ this.state.detailsOpen }>

            <div id="collapse-item-details">
              <ItemList products={ this.props.products } discounts={ this.props.discounts } />
            </div>

          </Collapse>
        </Card.Body>
        <Card.Body>
          <Card.Title as="h6">Add Promo Code<i><MDBIcon icon="plus"
            onClick={() => this.setState({ discountOpen: !this.state.discountOpen })} 
            aria-controls="collapse-discount"
            aria-expanded={ this.state.discountOpen } className="ml-2" /></i></Card.Title>
          <Collapse in={ this.state.discountOpen }>

            <div id="collapse-discount">
              <Discount />
            </div>
            
          </Collapse>
        </Card.Body>
      </Card>
    )
  }
}

const mapStateToProps = (state) => {
  // take in state from redux store
  // Return props obj for component to use
  return {
    discounts: state.discounts,
    products: state.products,
    tax: state.tax,
    shippingFee: state.shippingFee
  }
}


export default connect(mapStateToProps)(Checkout)