import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { applyPromo } from '../actions/promoActions';

class Discount extends Component {
  constructor(props) {
    super(props);

    this.state = {
      promoCode: null
    }
  }

  handleChange = (e) => {
    this.setState({
      promoCode: e.target.value
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { promoCode } = this.state;
    if(promoCode === 'DISCOUNT') {
      this.props.applyPromo(this.state.promoCode);
    } else {
      console.log("This discount code is invalid")
    }
  }

  render() {
    return (
      <Form onSubmit={ this.handleSubmit }>
        <Form.Group>
          <Form.Label>Promo Code</Form.Label>
          <Form.Control required type="text" placeholder="Enter promo code" onChange={ this.handleChange } />
        </Form.Group>
        <Button variant="primary" type="submit">
          Apply Now
        </Button>
      </Form>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    applyPromo: (promoCode) => dispatch(applyPromo(promoCode))
  }
}


export default connect(null,mapDispatchToProps)(Discount)