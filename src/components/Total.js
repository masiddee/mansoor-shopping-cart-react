import React from 'react';
import { Table } from 'react-bootstrap';

const Total = (props) => {
  const { discounts, products, tax, shippingFee } = props;

  // Calculate total price from all products in products array
  const subtotal = products.reduce((accumulator,currValue) => {
    return {price: ((accumulator.price * accumulator.quantity) + (currValue.price * currValue.quantity)).toFixed(2)};
  });

  const discAmount = discounts[1] ? (discounts[1].amount * subtotal.price) : null;
  const discPrice = discAmount ? subtotal.price - discAmount - discounts[0].amount : subtotal.price - discounts[0].amount;
  const taxAndFees = (tax * parseFloat(discPrice)) + shippingFee;
  // console.log(tax, shippingFee, discPrice, taxAndFees)

  return (
    <Table className="total-table">
      <tbody>
        <tr>
          <td>Subtotal:</td>
          <td><span>{ '$' + subtotal.price }</span></td>
        </tr>
        { discounts[1] && <tr><td>Promo Discount:</td><td><span className="text-danger">{ '-$' + discAmount.toFixed(2) }</span></td></tr> }
        <tr>
          <td><span>Pickup Savings:</span></td>
          <td><span className="text-danger">{ '-$' + (discounts[0].amount).toFixed(2) }</span></td>
        </tr>
        <tr>
          <td>Est. taxes &amp; fees:</td>
          <td><span>{ '$' + taxAndFees.toFixed(2) }</span></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td><h4>Est. Total:</h4></td>
          <td><h4>{ '$' + (discPrice + taxAndFees).toFixed(2) }</h4></td>
        </tr>
      </tfoot>
    </Table>
  )
}

export default Total
