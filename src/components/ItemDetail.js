import React from 'react';
import { connect } from 'react-redux';

const ItemDetail = (props) => {
  const { discounts, product } = props;

  const regPrice = product.price * product.quantity;
  const discPrice = discounts[1] ? regPrice - (discounts[1].amount * regPrice) : regPrice;

  return (
    <div className="product-details">
      <img src={ product.img } alt="Product" />
      <div>
        <h6 className="product-details-title">{ product.name }</h6>
        <span className="product-details-price">{ discounts[1] ? '$' + discPrice.toFixed(2) : '$' + regPrice.toFixed(2) }</span> <br />
        { discounts[1] && <span className="product-details-strikeprice">{ '$' + regPrice.toFixed(2) }<br /></span> }
        <span>Qty: { product.quantity }</span>
      </div>
    </div>
  )
}

export default ItemDetail
