import React from 'react'
import { ListGroup } from 'react-bootstrap';
import ItemDetail from './ItemDetail';

const ItemList = (props) => {
  const { products, discounts } = props;

  return (
    <ListGroup>
      <ListGroup.Item>
        { products && products.map(product => {
            return (
              <ItemDetail product={ product } key={ product.id } discounts={ discounts } />
            )
        }) }
      </ListGroup.Item>
    </ListGroup>
  )
}

export default ItemList
